package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		try(InputStream is = new FileInputStream("app.properties")){
			Properties properties = new Properties();
			properties.load(is);
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));

		} catch(SQLException | FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
		return instance = new DBManager();
	}



	public List<User> findAllUsers() throws DBException {
		getInstance();
		String SelectUsersQuery ="SELECT * FROM users";
		List<User> users= new ArrayList<>();
		try (Statement ps = connection.createStatement()) {
			ResultSet rs = ps.executeQuery(SelectUsersQuery);
			while (rs.next()) {
				users.add(User.createUser(rs.getString("login")));
			}
		}catch(SQLException e){
				throw new DBException("Exception", e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		getInstance();
		String InsertUserQuery = "INSERT INTO users (login) VALUES (?)";

		try (PreparedStatement ps = connection.prepareStatement(InsertUserQuery)){
  			ps.setString(1, user.getLogin());
			int changedLines = ps.executeUpdate();
			user.setId(getUser(user.getLogin()).getId());

			if(changedLines != 1) throw new SQLException();
		}catch(SQLException e){
			e.printStackTrace();
			throw new DBException("Exception", e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return true;
	}


	public boolean deleteUsers(User... users) throws DBException {
		getInstance();
		String deleteUsersQuery = "DELETE FROM users WHERE login = ?";

		try(PreparedStatement ps = connection.prepareStatement(deleteUsersQuery)) {
			for (User user : users) {
				ps.setString(1, user.getLogin());
			}
			ps.executeUpdate();
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		getInstance();
		String getUserQuery = "SELECT * FROM users WHERE login = ?";
		User user = null;


		try(PreparedStatement ps = connection.prepareStatement(getUserQuery)){
			ps.setString(1, login);
			ResultSet rs = ps.executeQuery();
			rs.next();
			user = User.createUser(rs.getString("login"));
			user.setId(rs.getInt("id"));
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		getInstance();
		String getTeamQuery = "SELECT * FROM teams WHERE name = ?";
		Team team = null;


		try(PreparedStatement ps = connection.prepareStatement(getTeamQuery)){
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			rs.next();
			team = Team.createTeam(rs.getString("name"));
			team.setId(rs.getInt("id"));
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		getInstance();
		String SelectTeamsQuery ="SELECT * FROM teams";
		List<Team> teams= new ArrayList<>();
		try (Statement ps = connection.createStatement()) {
			ResultSet rs = ps.executeQuery(SelectTeamsQuery);
			while (rs.next()) {
				teams.add(Team.createTeam(rs.getString("name")));
			}
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		getInstance();
		String InsertTeamQuery = "INSERT INTO teams (name) VALUES (?)";

		try (PreparedStatement ps = connection.prepareStatement(InsertTeamQuery)){
			ps.setString(1, team.getName());
			int changedLines = ps.executeUpdate();
			team.setId(getTeam(team.getName()).getId());
			if(changedLines != 1) throw new SQLException();
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		getInstance();
		String query = "INSERT INTO users_teams VALUES(?,?)";

		try(PreparedStatement ps = connection.prepareStatement(query)) {
			connection.setAutoCommit(false);
			ps.setInt(1, user.getId());
			for(Team team : teams){
				ps.setInt(2, team.getId());
				ps.executeUpdate();
			}
			connection.commit();
		}catch(SQLException e){
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException("Exception", e);
			}
			e.printStackTrace();
			throw new DBException("Exception", e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		getInstance();
		String teamIdQuery = "SELECT * FROM users_teams WHERE user_id = ?";
		List<Integer> userTeamsId = new ArrayList<>();
		String teamQuery = "SELECT * FROM teams WHERE id = ?";
		List<Team> userTeams = new ArrayList<>();

		try(PreparedStatement ps = connection.prepareStatement(teamIdQuery);
		PreparedStatement ps2 = connection.prepareStatement(teamQuery);) {
			ps.setInt(1, user.getId());
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				userTeamsId.add(rs.getInt("team_id"));
			}

			for(Integer id : userTeamsId){
				ps2.setInt(1, id);
				ResultSet rs2 = ps2.executeQuery();
				rs2.next();
				Team team = Team.createTeam(rs2.getString("name"));
				team.setId(id);
				userTeams.add(team);
			}
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		getInstance();
		String deleteUsersQuery = "DELETE FROM teams WHERE name = ?";

		try(PreparedStatement ps = connection.prepareStatement(deleteUsersQuery)) {
			ps.setString(1, team.getName());
			ps.executeUpdate();
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		getInstance();
		String UpdateTeamQuery = "UPDATE teams SET name = ? WHERE id = ?";
		try (PreparedStatement ps = connection.prepareStatement(UpdateTeamQuery)) {
			ps.setString(1, team.getName());
			ps.setInt(2, team.getId());
			ps.executeUpdate();
		}catch(SQLException e){
			throw new DBException("Exception", e);
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}
